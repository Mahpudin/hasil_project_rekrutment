<!--
=========================================================
* Soft UI Design System - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/soft-ui-design-system
* Copyright 2021 Creative Tim (https://www.creative-tim.com)

Coded by www.creative-tim.com

 =========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="apple-touch-icon" sizes="76x76" href="./assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="./assets/img/favicon.png">
  <title>
   Aplikasi Cek Resi 
  </title>
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
  <!-- Nucleo Icons -->
  <link href="./assets/css/nucleo-icons.css" rel="stylesheet" />
  <link href="./assets/css/nucleo-svg.css" rel="stylesheet" />
  <!-- Font Awesome Icons -->
  <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
  <link href="./assets/css/nucleo-svg.css" rel="stylesheet" />
  <!-- CSS Files -->
  <link id="pagestyle" href="./assets/css/soft-design-system.css?v=1.0.0" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <!-- <link href="./assets/demo/demo.css" rel="stylesheet" /> -->
</head>

<body class="inputs-sections">
  
  <div class="container mt-5">
    <div class="row">
      <div class="col-lg-12 mx-auto">
        <div class="mb-4 w-25">
          <nav aria-label="breadcrumb">
 
          </nav>
          <h3>CEK RESI</h3>
        </div>
        <div class="position-relative border-radius-xl overflow-hidden shadow-lg mb-7">
          <div class="container border-bottom">
            <div class="row justify-space-between py-2">
              <div class="col-lg-3 me-auto">
                <p class="lead text-dark pt-1 mb-0">Form Cek Resi</p>
              </div>

            </div>
          </div>
          <div class="tab-content tab-space">
            <div class="tab-pane active" id="preview-input-simple">
        <section class="py-7">
  <div class="container">
    <div class="row justify-space-between py-2">
      <div class="col-lg-6 mx-auto">
        <form action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
        <input type="text" name="noresi" placeholder="Input Resi" class="form-control" >

        <div class="form-check">
          <input class="form-check-input" name="jenis_expedisi" value="jne" type="radio" name="flexRadioDefault" id="flexRadioDefault1" checked>
          <label class="form-check-label" for="flexRadioDefault1">
           JNE
          </label>
        </div>
        <div class="form-check">
          <input class="form-check-input" name="jenis_expedisi" value="tiki" type="radio" name="flexRadioDefault" id="flexRadioDefault2">
          <label class="form-check-label" for="flexRadioDefault2">
           TIKI
          </label>
        </div>
        <div class="form-check">
          <input class="form-check-input" name="jenis_expedisi" value="sicepat" type="radio" name="flexRadioDefault" id="flexRadioDefault3">
          <label class="form-check-label" for="flexRadioDefault2">
            SI CEPAT
          </label>
        </div>
        <div class="form-check">
          <input class="form-check-input" name="jenis_expedisi" value="jnt" type="radio" name="flexRadioDefault" id="flexRadioDefault4">
          <label class="form-check-label" for="flexRadioDefault2">
            JNT
          </label>
        </div>
        <div class="form-check">
          <input class="form-check-input" name="jenis_expedisi" value="antaraja" type="radio" name="flexRadioDefault" id="flexRadioDefault5">
          <label class="form-check-label" for="flexRadioDefault2">
            ANTARAJA
          </label>
        </div>
        <div class="form-check">
          <input class="form-check-input" name="jenis_expedisi" value="pos" type="radio" name="flexRadioDefault" id="flexRadioDefault6">
          <label class="form-check-label" for="flexRadioDefault2">
            POS
          </label>
        </div>

  
      </div>
      <div class="col-lg-3 mx-auto">
        <button type="submit" name="submit" class="btn bg-gradient-dark w-100">CEK</button>
      </div>
    </form>
    </div>

<?php
     if (isset($_POST['submit'])) {

      $noresi = $_POST['noresi'];
      $expedisi = $_POST['jenis_expedisi'];
?>

    <table class="table">
      <thead>
        <tr>
 
          <th scope="col">Nomor Resi</th>
          <th scope="col">Tanggal Pengiriman</th>
          <th scope="col">Pengirim</th>
          <th scope="col">Penerima</th>
        </tr>
      </thead>
      <tbody>
        <?php 


        
          $url="https://api.binderbyte.com/v1/track?api_key=5d3f244ae1e384c1f1e87a258d7a10b73b599ac3c43896fac29e42776abd1e13&courier=$expedisi&awb=$noresi";
          
          $curl = curl_init();
          curl_setopt($curl, CURLOPT_URL, $url);
          curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
          $response = curl_exec($curl);
          curl_close($curl);
          $data =  json_decode($response, true);
          $resi = $data['data']['summary']['awb'];
          $datatgl =  $data['data']['summary']['date'];
          $convertstr = strtotime($datatgl);
          $tgl = date('D M Y H:i:s', $convertstr);
          $pengirim = $data['data']['detail']['shipper'];
          $penerima = $data['data']['detail']['receiver'];
          echo "<tr>";
          echo "<td>$resi</td>";
          echo "<td>$tgl</td>";
          echo "<td>$pengirim</td>";
          echo "<td>$penerima</td>";
          echo "</tr>";

      

        ?>
     
      </tbody>
    </table>

    <h3>Detail Pengiriman</h3>

    <table class="table">
      <thead>
        <tr>
 
          <th scope="col">Tanggal/Waktu</th>
          <th scope="col">Deskripsi</th>
        </tr>
      </thead>
      <tbody>
        <?php 
        
          $url="https://api.binderbyte.com/v1/track?api_key=5d3f244ae1e384c1f1e87a258d7a10b73b599ac3c43896fac29e42776abd1e13&courier=sicepat&awb=000789837988";
          
          $curl = curl_init();
          curl_setopt($curl, CURLOPT_URL, $url);
          curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
          $response = curl_exec($curl);
          curl_close($curl);
          $data =  json_decode($response, true);
          $resi = $data['data']['history'];
          foreach ($resi as $row)
          {
            $data_tgl_pengiriman = $row['date'];
            $convertstrp = strtotime($data_tgl_pengiriman);
            $tgl_pengiriman = date('D M Y H:i:s', $convertstrp);
            $desc = $row['desc'];
            echo "<tr>";
            echo "<td>$tgl_pengiriman</td>";
            echo "<td>$desc</td>";
            echo "</tr>";
          }

        ?>
     
      </tbody>
    </table>
  <?php
  {
       }
  }
?>
  </div>
</section>
</div>
            <div class="tab-pane" id="code-input-simple">
              <div class="position-relative p-4 pb-2">
                <a class="btn btn-sm bg-gradient-dark position-absolute end-4 mt-3" onclick="copyCode(this);" type="button"><i class="fas fa-copy text-sm me-1"></i> Copy</a>
                <figure class="highlight"><pre><code class="language-html" data-lang="html"><span class="nt">&lt;section</span> <span class="na">class=</span><span class="s">"py-7"</span><span class="nt">&gt;</span>
  <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"container"</span><span class="nt">&gt;</span>
    <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"row justify-space-between py-2"</span><span class="nt">&gt;</span>
      <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"col-lg-4 mx-auto"</span><span class="nt">&gt;</span>
        <span class="nt">&lt;input</span> <span class="na">type=</span><span class="s">"text"</span> <span class="na">placeholder=</span><span class="s">"Regular"</span> <span class="na">class=</span><span class="s">"form-control"</span> <span class="nt">&gt;</span>
      <span class="nt">&lt;/div&gt;</span>
    <span class="nt">&lt;/div&gt;</span>
  <span class="nt">&lt;/div&gt;</span>
<span class="nt">&lt;/section&gt;</span></code></pre>
                </figure>
              </div>
            </div>
          </div>
        </div>

      

  <script src="../../assets/js/core/popper.min.js" type="text/javascript"></script>
  <script src="../../assets/js/core/bootstrap.min.js" type="text/javascript"></script>>
  <!-- -------- END FOOTER 3 w/ COMPANY DESCRIPTION WITH LINKS & SOCIAL ICONS & COPYRIGHT ------- -->
  <!--   Core JS Files   -->
  <script src="../../assets/js/core/popper.min.js" type="text/javascript"></script>
  <script src="../../assets/js/core/bootstrap.min.js" type="text/javascript"></script>
  <script src="../../assets/js/plugins/perfect-scrollbar.min.js"></script>
  <script src="../../assets/js/plugins/prism.min.js"></script>
  <!--  Plugin for Parallax, full documentation here: https://github.com/wagerfield/parallax  -->
  <script src="../../assets/js/plugins/parallax.min.js"></script>
  <!-- Control Center for Soft UI Kit: parallax effects, scripts for the example pages etc -->
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDTTfWur0PDbZWPr7Pmq8K3jiDp0_xUziI"></script>
  <script src="../../assets/js/soft-design-system.min.js?v=1.0.0" type="text/javascript"></script>
</body>

</html>